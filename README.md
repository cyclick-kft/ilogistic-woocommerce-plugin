=== iLogistic Fulfillment WooCommerce Plugin ===

Tags: fulfillment, ilogistic, woocommerce

Requires PHP: 8.0

Requires at least: 5.6.0

Tested up to: 6.2.0

Stable tag: 4.3.3

License: GPLv3

Contributors: ilogdev

[License URI](LICENSE)

== Description ==

Az iLogistic egy Fulfillment As A Service szolgáltás, mely a te webshop-odhoz tartozó raktár készlet kezelését,
illetve csomagolást és csomag feladást hivatott elvégezni.

A plugin telepítését és beállítását követően, új termékeid és rendeléseid azonnal vagy periodikusan el lesznek küldve
az iLogistic alkalmazása felé.

Termék készleteid pedig lekérdezésre kerülnek az iLogistic-től, amennyiben ez az opció mind a raktárkezelő felületén,
mind az adott terméken be van kapcsolva.

Ezek szinkronizálásához a webshop-odból időzített műveletekkel és eseményekkel API hívásokat intéz a beépülő az
iLogistic REST API felé.

**A plugin használatához regisztrált és aktivált iLogistic felhasználó szükséges!**

A teljes dokumentációért látogasson el
az [iLogistic API-okkal kapcsolatos weboldalára.](https://api.ilogistic.eu/documentation/docs/wc-plugin)

Bármilyen sérülékenységet, hibát talál, vagy csak kérdése van, keressen minket bizalommal a
következő [elérhetőségeken](http://api.ilogistic.eu/documentation/help).

== Changelog ==

[A változások a CHANGELOG.md fájlban találhatóak](CHANGELOG.md)
