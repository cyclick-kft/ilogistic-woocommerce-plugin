# 4.6.1
- Fixed a bug where some cron schedules did not register

# 4.6.0
- Added express one integration

# 4.5.1
- Removed whitespaces

# 4.5.0
- Added a curl timeout retry mechanism to the order update and create functions
- Added new function to manually set frequency of the order and/or product cronjobs
- Moved cron registration to a separate function
- Added support for different priced, but same products in the same order

# 4.4.0
- added support for new woocommerce version
- added retry function if api call fails with 5xx error
- changed auth token generation cronjob to 2 days

# 4.3.5
- Add priority to order create/update hooks

# 4.3.4
- GLS shipping method update gls_shop, gls_locker

# 4.3.3
- Refactor order create and update logic + extend logging to orders with `0` as `ilogistic_id`

# 4.3.2
- Extend logging for empty orders

# 4.3.1
- Fix pagination for orders and products it is now able to reach the last page

# 4.3.0
- Add support for resent orders
- Fix product stock updates for more tan 100 products

# 4.2.0
- Add support for Viszt Péter's MPL points
- Change product update to run in 100 product / request batches

# 4.1.1
- Change order status filters in order update, to exclude statuses that match with one of the final state

# 4.1.0
- Added compatibility for two WooCommerce Stripe plugins
- Added the two Pactic shipping method
- Updated logging on failed requests

# 4.0.0
- PHP required version increased to 8.0
- Added support for virtual products

# 3.2.0
- Added shop alias handling for multiple webshops.
- Changed order status update's source to ilogistic.

# 3.1.0
- Add new shipping methods (iLogistic csomagpont, DHL, MPL, Raklapos szállítás)

# 3.0.1
- Changed product barcode type to array

# 3.0.0
- Add unregister cleanup hooks
- Add support for Viszt Péter's package point plugin
- Remove thank_you page hook
- From now on orders only sync with a mandatory 5 min cronjob.

# 2.0.3
- Reduce the number of logs, fix stock logs

# 2.0.2
- Remove implicit limit from cron order push

# 2.0.1
- Fixed the new logging service to prevent errors while sending new orders through the API

# 2.0.0
- Fixed 24H product push
- Fixed order push before install
- Refactored logging
- Changed order price calculation to a simple getter + price rounding

# 1.10.0
Fixed 0 product sync, added product sync to every 24h as a cron job.

# 1.9.6
Fixed status filters on order status update.

# 1.9.5
Removed insertion 0 for ilogistic_id to avoid unnecessary GET requests. 

# 1.9.4
Products that have at least one variable, now only handled by the specific variant in the orders, not as a standard product.
Product variations are showing handled correctly in orders.

# 1.9.3
Adjusted log levels to match the cause.

# 1.9.2
Code cleanup/standardization.

# 1.9.1
Fixed variable/bundle products stock sync method.

# 1.9.0

From now on variable products and bundle products (created with: WPC Product Bundles for WooCommerce plugin), can be
pushed to the iLogistic warehouse management system.

# 1.8.0

Extended shipping method handling with new statuses: `Csomaküldő`, `Személyes átvétel`.

# 1.7.0

Extended status handling with new statuses: `Kiszállítva`, `Csomagponton várakozik`, `Visszáru`, `Szállítás közben megsérült`.

# 1.6.8

Validation tweaks to prevent PHP warnings, increased timout time on stock updates. 

# 1.6.7

Fixed condition that determines product is new.

# 1.6.6

Fixed an issue with checking for status updates only affected the first 10 orders.

# 1.6.5

Fixed PHP exception upon using `push_product()` function and receiving `WP_Error` object as response. (Thrown when WP
can't reach the target host.)

# 1.6.4

Fixed null return value on remote API requests to update stocks.

# 1.6.3

Tested with WooCommerce 5.0. Added 3 sec leeway for the validation in the order creation, details attached to some
events as logs.

# 1.6.2

Fixed that stock update only applies for the first page of products, instead of every product.

# 1.6.1

Added new field for storing WooCommerce ID, added new status checks for newly created orders.

# 1.5.4

Fixed `null` calls and failing token generation errors.

# 1.5.3

Fixed shipping properties update from the WooCommerce to the iLogistic.

# 1.5.2

Fixed setting product stock to 0 when no data returned, but fetaure enabled.

# 1.5.1

Fixed settings buttons on versioned links.

# 1.5.0

Introduced a new feature, update stock managed product's stocks.

# 1.4.1

Updated `Jóváhagyva/Jóváhagyásra vár` status, to a multiselect.

# 1.4.0

Introduced a new feature to upload orders that has been submitted before the installation. It has a settings option to
enable this feature.

# 1.3.0

Introduced custom log file, with logfmt formated messages.

# 1.2.4

Improved logging capabilities.

# 1.2.3

Hotfix: Updated link position under the Plugins tab.

# 1.2.2

Hotfix: Removed `JSON_NUMERIC_CHECK` option, forced string cast on SKUs to avoid bad conversion.

# 1.2.1

Hotfix: Removed exit from redirect.

# 1.2.0

Added new settings menu to plugin settings and settings redirection to plugin activation.

# 1.1.1

Fixed default or bad shipping/payment method handling.

# 1.1.0

Added handling of custom Shipping and Payment methods.

# 1.0.2

Shipping data filled out for iLogistic system on digital orders as well.

# 1.0.1

Shipping company added for orders with only digital products.

# 1.0.0

Initial release.