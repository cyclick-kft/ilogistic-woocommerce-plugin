<?php

namespace Ilogistic;

class Cron {
	const SCHEDULES = [
		'every_5_min'  => [
			'html_select' => [ '5 perc', 'cron timing', 'ilogistic' ],
			'cron'        => [
				'interval' => 300,
				'display'  => 'Every 5 minutes'
			]
		],
		'every_10_min' => [
			'html_select' => [ '10 perc', 'cron timing', 'ilogistic' ],
			'cron'        => [
				'interval' => 600,
				'display'  => 'Every 10 minutes'
			]
		],
		'every_30_min' => [
			'html_select' => [ '30 perc', 'cron timing', 'ilogistic' ],
			'cron'        => [
				'interval' => 1800,
				'display'  => 'Every 30 minutes'
			]
		],
		'every_hour'   => [
			'html_select' => [ '1 óra', 'cron timing', 'ilogistic' ],
			'cron'        => [
				'interval' => 3600,
				'display'  => 'Every hour'
			]
		],
		'every_2_hour' => [
			'html_select' => [ '2 óra', 'cron timing', 'ilogistic' ],
			'cron'        => [
				'interval' => 7200,
				'display'  => 'Every 2 hours'
			]
		]
	];

	const UPDATE_PRODUCT_CRON = 'ilogistic_wc_update_stock';
	const UPDATE_ORDER_CRON = 'ilogistic_wc_update_order';
	const ISSUE_AUTH_TOKEN = 'ilogistic_issue_auth_token';
	const PUSH_ALL_PRODUCTS = 'ilogistic_push_all_products';

	private static ?array $htmlSelectData = null;
	private static ?array $cronData = null;

	private static function transformData(): void {
		$htmlSelectData = [];
		$timingData     = [];

		foreach ( self::SCHEDULES as $key => $value ) {
			$htmlSelectData[ $key ] = _x( ...$value['html_select'] );

			$value['cron']['display'] = __( $value['cron']['display'] );
			$timingData[ $key ]       = $value['cron'];
		}

		self::$htmlSelectData = $htmlSelectData;
		self::$cronData       = $timingData;
	}

	public static function getForHtmlSelect(): array {
		if ( ! self::$htmlSelectData ) {
			self::transformData();
		}

		return self::$htmlSelectData;
	}

	public static function getForWpCron(): array {
		if ( ! self::$cronData ) {
			self::transformData();
		}

		return self::$cronData;
	}

	public static function clearCrons(): void {
		foreach ( [ self::UPDATE_ORDER_CRON, self::UPDATE_PRODUCT_CRON ] as $value ) {
			wp_clear_scheduled_hook( $value );
		}
	}

	public static function registerToOrderIfCronNotExists(): void {
		if ( ! wp_next_scheduled( self::UPDATE_ORDER_CRON ) ) {
			self::setToOrder( 'every_5_min' );
		}
	}

	public static function registerToProductIfCronNotExists(): void {
		if ( ! wp_next_scheduled( self::UPDATE_PRODUCT_CRON ) ) {
			self::setToProduct( 'every_5_min' );
		}
	}

	public static function setToOrder( string $recurrence ): void {
		wp_schedule_event( time(), $recurrence, self::UPDATE_ORDER_CRON );
	}

	public static function setToProduct( string $recurrence ): void {
		wp_schedule_event( time(), $recurrence, self::UPDATE_PRODUCT_CRON );
	}

	public static function createIssuaAuthTokenCron(): void {
		if( ! wp_next_scheduled( self::ISSUE_AUTH_TOKEN ) ) {
			wp_schedule_event( time(), 'every_2_days', self::ISSUE_AUTH_TOKEN );
		}
	}

	public static function createPushAllProductsCron(): void {
		if( ! wp_next_scheduled( self::PUSH_ALL_PRODUCTS ) ) {
			wp_schedule_event( time(), 'daily', self::PUSH_ALL_PRODUCTS );
		}
	}
}