<?php


namespace Ilogistic\Hooks;


use Ilogistic\Api_Service;

class Authentication_Hooks {
	/**
	 * Registers the hooks.
	 */
	public static function register() {
		add_filter( 'cron_schedules', self::class . '::register_cron_every_2_days' );
		add_action( 'ilogistic_issue_auth_token', self::class . '::get_new_auth_token' );
		add_action( 'wp', self::class . '::register_new_cron' );
		add_action( 'update_option_ilogistic_token', self::class . '::validate_auth_token', 10, 2 );
	}

	public static function unregister() {
		remove_filter( 'cron_schedules', self::class . '::register_cron_every_2_days' );
		remove_action( 'ilogistic_issue_auth_token', self::class . '::get_new_auth_token' );
		remove_action( 'wp', self::class . '::register_new_cron' );
		remove_action( 'update_option_ilogistic_token', self::class . '::validate_auth_token' );
	}

	/**
	 * Adds a new cron job to the cron job schedules which will run every 2 days.
	 *
	 * @param $schedules array Current cron schedules
	 *
	 * @return array Updated cron schedules
	 */
	public static function register_cron_every_2_days( array $schedules ): array {
		$schedules['every_2_days'] = array(
			'interval' => 172800,
			'display'  => __( 'Every 2 days.' )
		);

		return $schedules;
	}

	/**
	 * Creates a new hook to get a new auth token and update it in the config.
	 */
	public static function get_new_auth_token() {
		$api_service = Api_Service::get_service();
		$token       = $api_service->get_new_token( esc_attr( get_option( 'ilogistic_token' ) ) );
		if ( $token !== '' ) {
			update_option( 'ilogistic_token', $token );
		}
	}

	/**
	 * Registers the hook above ^ to run at the 'every_2_days' cronjob, if not already registered for it.
	 */
	public static function register_new_cron() {
		if ( ! wp_next_scheduled( 'ilogistic_issue_auth_token' ) ) {
			wp_schedule_event( time(), 'every_2_days', 'ilogistic_issue_auth_token' );
		}
	}

	/**
	 * @param string $old_token
	 * @param string $new_token
	 */
	public static function validate_auth_token( string $old_token, string $new_token ) {
		$api_service = Api_Service::get_service();
		$is_valid    = $api_service->validate_token( $new_token );
		if ( $is_valid ) {
			Product_Hooks::register_all_products();
		}
	}

}