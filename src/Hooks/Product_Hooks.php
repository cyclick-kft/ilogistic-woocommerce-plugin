<?php

namespace Ilogistic\Hooks;

use DateTime;
use Ilogistic\Api_Service;
use Ilogistic\Cron;
use Ilogistic\Logfmt_Log_Handler_File;
use Ilogistic\Model\Product;
use WC_Logger;
use WC_Product;

class Product_Hooks {
	/**
	 * Registers the hooks.
	 */
	public static function register(): void {
		add_action( 'woocommerce_update_product', self::class . '::create_ilogistic_product', 10, 1 );
		add_action( 'wp', self::class . '::ilogistic_update_trigger' );
		add_action( 'ilogistic_wc_update_stock', self::class . '::update_all_product_stock' );
		add_action( 'ilogistic_push_all_products', self::class . '::register_all_products' );
		add_filter( 'cron_schedules', self::class . '::create_every_24_hours_cron' );
	}

	public static function unregister(): void {
		remove_action( 'woocommerce_update_product', self::class . '::create_ilogistic_product' );
		remove_action( 'wp', self::class . '::ilogistic_update_trigger' );
		remove_action( 'ilogistic_wc_update_stock', self::class . '::update_all_product_stock' );
		remove_action( 'ilogistic_push_all_products', self::class . '::register_all_products' );
		remove_filter( 'cron_schedules', self::class . '::create_every_24_hours_cron' );
	}


	/**
	 * @param array $schedules
	 *
	 * @return array
	 */
	public static function create_every_24_hours_cron( array $schedules ): array {
		$schedules['every_24_hours'] = [
			'interval' => 86400,
			'display'  => __( 'Every 24 hours' )
		];

		return $schedules;
	}

	/**
	 * @param int $product_id WC_Product The created/updated WooCommerce product ID
	 * Gets the new product and send it to the API, if there is a cost and SKU field.
	 * Happens when a WC product is created/modified.
	 */
	public static function create_ilogistic_product( int $product_id ): void {
		$log_handler = new Logfmt_Log_Handler_File();
		$logger      = new WC_Logger( [ $log_handler ] );
		$product     = wc_get_product( $product_id );

		// do something with this product
		if ( $product->get_sku() === '' && $product->has_child() === false ) {
			$logger->error(
				"failed to push product no SKU presented",
				[ 'woo_id' => $product_id ]
			);

			return;
		}

		if ( $product->get_price() === '' ) {
			$logger->warning(
				"product was sent with price 0!",
				[ 'woo_id' => $product_id ]
			);
		}

		$created_date = $product->get_date_created();

		if ( $product->has_child() ) {
			foreach ( $product->get_children() as $child ) {
				$parsed_product = Product::create_from_woocommerce_product( wc_get_product( $child ) );
				Api_Service::get_service()
				           ->push_product( $parsed_product, esc_attr( get_option( 'ilogistic_token' ) ) );
			}
		} else {
			if (
				is_null( $created_date ) === false
				&& ( ( new DateTime( '-5 minutes' ) )->getTimestamp() <= $created_date->getTimestamp() )
			) {
				$parsed_product = Product::create_from_woocommerce_product( $product );
				Api_Service::get_service()
				           ->push_product( $parsed_product, esc_attr( get_option( 'ilogistic_token' ) ) );
			}
		}

	}

	/**
	 * Gets every WC_Product and generate an iLogistic product from it, then pushes it to the API
	 */
	public static function register_all_products(): void {
		$args                    = [ 'return' => 'ids', 'posts_per_page' => - 1 ];
		$product_ids             = wc_get_products( $args );
		$ilogistic_product_array = array();

		foreach ( $product_ids as $product_id ) {
			$product = wc_get_product( $product_id );

			if ( $product->get_sku() !== '' ) {
				$ilogistic_product_array[] = Product::create_from_woocommerce_product( $product );
			} else {
				if ( $product->has_child() ) {
					foreach ( $product->get_children() as $child ) {
						$parsed_product            = Product::create_from_woocommerce_product( wc_get_product( $child ) );
						$ilogistic_product_array[] = $parsed_product;
					}
				}
			}
		}
		$api_service = Api_Service::get_service();
		$api_service->push_products( $ilogistic_product_array, esc_attr( get_option( 'ilogistic_token' ) ) );
	}

	/**
	 * Creates an event that triggered to run every 5 minutes (see above)
	 */
	public static function ilogistic_update_trigger(): void {
		Cron::registerToProductIfCronNotExists();

		if ( ! wp_next_scheduled( 'ilogistic_push_all_products' ) ) {
			wp_schedule_event( time(), 'every_24_hours', 'ilogistic_push_all_products' );
		}
	}

	/**
	 * Updates all the public, stock managed product's stock.
	 */
	public static function update_all_product_stock(): void {
		$args             = array(
			'status'       => 'publish',
			'return'       => 'objects',
			'manage_stock' => true,
			'limit'        => 100,
			'paginate'     => true,
			'page'         => 1,
		);
		$managed_products = wc_get_products( $args );

		self::update_stock( $managed_products->products );

		for ( $i = 2; $i <= $managed_products->max_num_pages; $i ++ ) {
			$args['page']     = $i;
			$managed_products = wc_get_products( $args );
			self::update_stock( $managed_products->products );
		}
	}

	/**
	 * Updates a batch of products received as a param
	 *
	 * @param array<WC_Product> $products batch of products
	 *
	 * @return void
	 */
	private static function update_stock( array $products ): void {
		$updatable_products = array();
		$log_handler        = new Logfmt_Log_Handler_File();
		$logger             = new WC_Logger( [ $log_handler ] );
		foreach ( $products as $key => $product ) {
			if ( $product->has_child() === true ) {
				foreach ( $product->get_children() as $child ) {
					$child_product = wc_get_product( $child );
					if ( $child_product->is_virtual() === false ) {
						if ( $child_product->get_sku() !== '' ) {
							$updatable_products[] = $child_product->get_sku();
						} else {
							$logger->error(
								"can't update product stock!",
								[ 'woo_id' => $product->get_id(), 'reason' => 'no sku' ]
							);
						}
					}
				}
				continue;
			}

			if ( $product->is_virtual() === false ) {
				if ( $product->get_sku() !== '' ) {
					$updatable_products[] = $product->get_sku();
					continue;
				}

				unset( $products[ $key ] );

				$logger->error(
					"can't update product stock!",
					[ 'woo_id' => $product->get_id(), 'reason' => 'no sku' ]
				);
			}
		}

		$api_responses = Api_Service::get_service()->get_stock(
			$updatable_products,
			esc_attr( get_option( 'ilogistic_token' ) )
		);

		foreach ( $products as $product ) {
			$sku           = $product->get_sku();
			$prev_quantity = $product->get_stock_quantity();

			if ( $product->has_child() ) {
				foreach ( $product->get_children() as $child ) {
					$child_product       = wc_get_product( $child );
					$child_sku           = $child_product->get_sku();
					$child_prev_quantity = $child_product->get_stock_quantity();

					if ( isset( $api_responses[ $child_sku ] ) ) {
						$quantity = $api_responses[ $child_sku ];
						if ( $quantity !== $child_prev_quantity ) {
							$child_product->set_stock_quantity( $quantity );
							$logger->info(
								"updated product stock from $child_prev_quantity to {$quantity}",
								[ 'woo_id' => $product->get_id(), 'sku' => $child_sku ]
							);
							$child_product->save();
						}
					} else {
						$logger->error(
							"can't update product stock!",
							[ 'woo_id' => $product->get_id(), 'reason' => 'no sku', 'sku' => $child_sku ]
						);
					}
				}
			}

			if ( isset( $api_responses[ $sku ] ) ) {
				$quantity = $api_responses[ $sku ];
				if ( $quantity !== $prev_quantity ) {
					$product->set_stock_quantity( $quantity );
					$logger->info(
						"updated product stock from $prev_quantity to {$quantity}",
						[ 'woo_id' => $product->get_id(), 'sku' => $sku ]
					);
					$product->save();
				}
			}
		}
	}
}