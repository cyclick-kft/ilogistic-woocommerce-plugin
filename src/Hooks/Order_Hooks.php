<?php

namespace Ilogistic\Hooks;

use Automattic\WooCommerce\Utilities\FeaturesUtil;
use Ilogistic\Api_Service;
use Ilogistic\Cron;
use Ilogistic\Logfmt_Log_Handler_File;
use Ilogistic\Model\Order;
use WC_Logger;
use WC_Order;
use WC_Product;

class Order_Hooks {

	private static ?WC_Logger $logger = null;

	/**
	 * Registers the hooks.
	 */
	public static function register(): void {
		add_filter( 'cron_schedules', self::class . '::create_cron_timings' );
		add_action( 'wp', self::class . '::ilogistic_update_trigger' );
		add_action( 'ilogistic_wc_update_order', self::class . '::update_ilog_order_event' );
		add_action( 'before_delete_post', self::class . '::delete_order_event' );
		add_action( 'ilogistic_wc_update_order', self::class . '::upload_orders', 5 );
		add_action( 'ilogistic_wc_update_order', self::class . '::update_wc_order_event', 10 );
	}

	public function unregister(): void {
		remove_filter( 'cron_schedules', self::class . '::create_cron_timings' );
		remove_action( 'wp', self::class . '::ilogistic_update_trigger' );
		remove_action( 'ilogistic_wc_update_order', self::class . '::update_ilog_order_event' );
		remove_action( 'before_delete_post', self::class . '::delete_order_event' );
		remove_action( 'ilogistic_wc_update_order', self::class . '::upload_orders', 5 );
		remove_action( 'ilogistic_wc_update_order', self::class . '::update_wc_order_event', 10 );
	}

	private static function get_logger(): WC_Logger {
		if ( is_null( self::$logger ) ) {
			$log_handler  = new Logfmt_Log_Handler_File();
			self::$logger = new WC_Logger( [ $log_handler ] );
		}

		return self::$logger;
	}

	/**
	 * Creates a new order and send it to the iLogistic API when a WC order is created.
	 * (The created iLogistic order's id will be injected to the order as an 'ilogistic_id' custom field.)
	 *
	 * @param int $order_id
	 */
	public static function create_ilogistic_order( int $order_id ): void {
		$order             = new WC_Order( $order_id );
		$isProductEmptySku = false;

		foreach ( $order->get_items() as $item ) {
			$product_data = new WC_Product( $item->get_data()['product_id'] );
			if ( $product_data->get_sku() === "" ) {
				$wc_product = wc_get_product( $item->get_data()['variation_id'] );
				if ( $wc_product === false || $wc_product->get_sku() === "" ) {
					$isProductEmptySku = true;
				}
			}
			unset( $product_data );
		}
		if ( $isProductEmptySku ) {
			self::get_logger()->error(
				"order push failed, order contains a product without SKU",
				[ 'woo_id' => $order_id ]
			);

			return;
		}
		$order->add_meta_data( 'ilogistic_id', 0, true );
		$order->save();

		$parsed_order = Order::create_from_woocommerce_order( $order );
		$api_service  = Api_Service::get_service();

		$ilogistic_id = $api_service->push_order(
			$parsed_order,
			$order_id,
			esc_attr( get_option( 'ilogistic_token' ) ),
			esc_attr( get_option( 'ilogistic_alias' ) ?? "Internal API" )
		);
		if ( $ilogistic_id === null ) {
			$order->delete_meta_data( 'ilogistic_id' );
			$order->save();

			return;
		}
		if ( $ilogistic_id !== 0 ) {
			$order->add_meta_data( 'ilogistic_id', $ilogistic_id, true );
			$order->save();
		}
	}

	/**
	 * Add a new configurable cron job timings.
	 *
	 * @param $schedules array Current cron schedules
	 *
	 * @return array Updated cron schedules
	 */
	public static function create_cron_timings( array $schedules ): array {
		return Cron::getForWpCron();
	}

	/**
	 * Creates an event that triggered to run every 5 minutes (see above)
	 */
	public static function ilogistic_update_trigger(): void {
		Cron::registerToOrderIfCronNotExists();
	}

	/**
	 * Register itself to the update event, get every order that changed in the last 5 minutes
	 */
	public static function update_ilog_order_event(): void {
		global $wpdb;
		$is_custom_order_feature_enabled = FeaturesUtil::feature_is_enabled( 'custom_order_tables' );
		if ( $is_custom_order_feature_enabled === true ) {
			$modified_orders = $wpdb->get_col( $wpdb->prepare(
				"SELECT orders.id
	                FROM {$wpdb->prefix}wc_orders AS orders
	                WHERE orders.date_updated_gmt >= '%s'
	                AND orders.date_updated_gmt <= '%s'",
				date( 'Y/m/d H:i:s', absint( strtotime( '-5 MINUTES', current_time( 'timestamp' ) ) ) ),
				date( 'Y/m/d H:i:s', absint( current_time( 'timestamp' ) ) )
			) );
		} else {
			$modified_orders = $wpdb->get_col( $wpdb->prepare(
				"SELECT posts.ID
	                FROM {$wpdb->prefix}posts AS posts
	                WHERE posts.post_type = 'shop_order'
	                AND posts.post_modified >= '%s'
	                AND posts.post_modified <= '%s'",
				date( 'Y/m/d H:i:s', absint( strtotime( '-5 MINUTES', current_time( 'timestamp' ) ) ) ),
				date( 'Y/m/d H:i:s', absint( current_time( 'timestamp' ) ) )
			) );
		}
		foreach ( $modified_orders as $modified_order ) {
			$order                = new WC_Order( $modified_order );
			$is_product_empty_sku = false;
			foreach ( $order->get_items() as $item ) {
				$productData = new WC_Product( $item->get_data()['product_id'] );
				if ( $productData->get_sku() === "" ) {
					$is_product_empty_sku = true;
				}
				unset( $productData );
			}
			if ( $is_product_empty_sku ) {
				self::get_logger()->error(
					"order push failed, order contains a product without SKU",
					[ 'woo_id' => $order->get_id() ]
				);
			} else {
				$ilog_order = Order::create_from_woocommerce_order( $order );
				$ilog_id    = $order->get_meta( 'ilogistic_id', true );
				Api_Service::get_service()->update_order(
					$ilog_order,
					$ilog_id,
					esc_attr( get_option( 'ilogistic_token' ) )
				);
			}
		}
	}

	/**
	 * Deletes the order from the iLogistic side, when deleted from the shop.
	 * (It will do nothing when the order has been put into the bin.)
	 *
	 * @param int $order_id iLogistic order id
	 */
	public static function delete_order_event( int $order_id ): void {
		global $post_type;

		if ( $post_type !== 'shop_order' ) {
			return;
		}

		$wc_order = new WC_Order( $order_id );
		$ilog_id  = $wc_order->get_meta( 'ilogistic_id', true );
		if ( $ilog_id !== "" ) {
			Api_Service::get_service()->delete_order( $ilog_id, esc_attr( get_option( 'ilogistic_token' ) ) );
		}
	}

	/**
	 * Gets the orders that aren't completed yet and has ilogistic_id in the WooCommerce shop
	 * @return WC_Order[]
	 */
	public static function orders_needed_to_check(): array {
		$jovahagyva_state = get_option( 'ilogistic_status_jovahagyva' );

		if ( is_array( $jovahagyva_state ) === false ) {
			$jovahagyva_state = [ $jovahagyva_state ];
		}

		$final_statuses = [ get_option( 'ilogistic_status_kiszallitva' ), get_option( 'ilogistic_status_visszaru' ) ];
		$order_statuses = array_filter(
			[
				get_option( 'ilogistic_status_osszekeszites' ),
				get_option( 'ilogistic_status_csomagolva' ),
				get_option( 'ilogistic_status_futarnak' ),
				get_option( 'ilogistic_status_csomagpont' ),
				get_option( 'ilogistic_status_serult' ),
				...$jovahagyva_state,
			],
			fn( $status ) => in_array( $status, $final_statuses, true ) === false,
		);

		$orders = wc_get_orders( [
			'date_created' => '>=' . date( strtotime( '-2 months' ) ),
			'limit'        => - 1,
			'status'       => $order_statuses
		] );

		foreach ( $orders as $key => $order ) {
			$ilogistic_id = $order->get_meta( 'ilogistic_id', true );
			if ( $ilogistic_id === "" || $ilogistic_id === 0 ) {
				unset( $orders[ $key ] );
			}
		}

		return $orders;
	}

	/**
	 * Collects and push orders if:
	 * - order is in the iLogistic starting status
	 * - order has no `ilogistic_id` property
	 */
	public static function upload_orders(): void {
		$args = [
			'status'   => get_option( 'ilogistic_status_jovahagyva' ),
			'limit'    => 100,
			'paginate' => true,
			'page'     => 1,
		];

		if ( is_array( get_option( 'ilogistic_status_jovahagyva' ) ) === false ) {
			$args['status'] = array( $args['status'] );
		}

		$orders = wc_get_orders( $args );

		foreach ( $orders->orders as $order ) {
			if ( $order->meta_exists( 'ilogistic_id' ) === false ) {
				self::create_ilogistic_order( $order->get_id() );
			}

			if ( $order->get_meta( 'ilogistic_id', true ) === '0' ) {
				self::get_logger()->warning(
					'order push already failed, remove ilogistic_id property to try again',
					[ 'woo_id' => $order->get_id() ]
				);
			}
		}

		for ( $i = 2; $i <= $orders->max_num_pages; $i ++ ) {
			$args['page'] = $i;
			$orders       = wc_get_orders( $args );

			foreach ( $orders->orders as $order ) {
				if ( $order->meta_exists( 'ilogistic_id' ) === false ) {
					self::create_ilogistic_order( $order->get_id() );
				}

				if ( $order->get_meta( 'ilogistic_id', true ) === '0' ) {
					self::get_logger()->warning(
						'order push already failed, remove ilogistic_id property to try again',
						[ 'woo_id' => $order->get_id() ]
					);
				}
			}
		}
	}

	/**
	 * Updates the order statuses that are modified in the iLogistic side
	 */
	public static function update_wc_order_event(): void {
		$statuses = array(
			//'Jóváhagyva'                 => get_option( 'ilogistic_status_jovahagyva' ),
			//'Jóváhagyásra vár'           => get_option( 'ilogistic_status_jovahagyva' ),
			'Összekészítés alatt'        => get_option( 'ilogistic_status_osszekeszites' ),
			'Csomagolva'                 => get_option( 'ilogistic_status_csomagolva' ),
			'Futárnak átadva'            => get_option( 'ilogistic_status_futarnak' ),
			'Csomagponton várakozik'     => get_option( 'ilogistic_status_csomagpont' ),
			'Szállítás közben megsérült' => get_option( 'ilogistic_status_serult' ),
			'Kiszállítva'                => get_option( 'ilogistic_status_kiszallitva' ),
			'Visszáru'                   => get_option( 'ilogistic_status_visszaru' ),
		);

		self::update_returned_orders();

		$wc_orders   = self::orders_needed_to_check();
		$api_service = Api_Service::get_service();
		foreach ( $wc_orders as $order ) {
			if ( $order->meta_exists( 'ilogistic_id' ) === false || $order->get_meta( 'ilogistic_id' ) === '0' ) {
				continue;
			}

			$ilog_order = $api_service->get_order(
				$order->get_meta( 'ilogistic_id', true ),
				esc_attr( get_option( 'ilogistic_token' ) )
			);
			if ( ! empty( $ilog_order['state'] ) && $statuses[ $ilog_order['state'] ] !== $order->get_status() ) {
				if ( ! is_null( $statuses[ $ilog_order['state'] ] ) ) {
					$order->set_status( $statuses[ $ilog_order['state'] ], 'iLogistic' );
					if ( $ilog_order['state'] === 'Visszáru' ) {
						$order->add_meta_data( 'update_to_resent_order', true );
					}
					$order->save();
					self::get_logger()->info(
						"order status updated",
						[ 'woo_id' => $order->get_id(), 'ilog_id' => $order->get_meta( 'ilogistic_id', true ) ]
					);
				}
			}
		}
	}

	public static function update_returned_orders(): void {
		$returned_state = get_option( 'ilogistic_status_visszaru' );

		if ( is_array( $returned_state ) === false ) {
			$returned_state = [ $returned_state ];
		}

		$orders = wc_get_orders( [
			'date_created' => '>=' . date( strtotime( '-2 months' ) ),
			'limit'        => - 1,
			'status'       => $returned_state,
			'meta_key'     => 'update_to_resent_order',
			'meta_value'   => true,
			'meta_compare' => '=',
		] );

		foreach ( $orders as $key => $order ) {
			$ilogistic_id = $order->get_meta( 'ilogistic_id', true );
			if ( empty( $ilogistic_id ) ) {
				unset( $orders[ $key ] );
			}
		}

		$api_service = Api_Service::get_service();
		foreach ( $orders as $order ) {
			$ilogistic_id = $order->get_meta( 'ilogistic_id', true );
			$response     = $api_service->get_resent_order( $ilogistic_id, esc_attr( get_option( 'ilogistic_token' ) ) );

			if ( is_null( $response ) === true || $response['status'] === "wait" ) {
				continue;
			}

			if ( $response['status'] === "resend" ) {
				$order->update_meta_data( 'ilogistic_id', $response['new_ilogistic_order_id'] );
				$new_status = get_option( 'ilogistic_status_jovahagyva' );
				if ( is_array( $new_status ) === true ) {
					$new_status = $new_status[0];
				}
				$order->set_status( $new_status, 'iLogistic' );
			}
			$order->delete_meta_data( 'update_to_resent_order' );
			$order->save();
		}
	}
}