<?php

namespace Ilogistic\Hooks;

use Ilogistic\Cron;

class Admin_Menu_Hooks {
	/**
	 * Registers all the hooks.
	 */
	public static function register() {
		add_filter( 'plugin_action_links', self::class . '::add_settings_link', 1, 2 );
		add_filter( 'woocommerce_settings_tabs_array', self::class . '::add_settings_tab', 50 );
		add_action( 'woocommerce_settings_tabs_ilogistic_settings_tab', self::class . '::settings_tab' );
		add_action( 'woocommerce_update_options_ilogistic_settings_tab', self::class . '::update_settings' );
	}

	public static function unregister() {
		remove_filter( 'plugin_action_links', self::class . '::add_settings_link', 1, 2 );
		remove_filter( 'woocommerce_settings_tabs_array', self::class . '::add_settings_tab', 50 );
		remove_filter( 'woocommerce_settings_tabs_ilogistic_settings_tab', self::class . '::settings_tab' );
		remove_filter( 'woocommerce_update_options_ilogistic_settings_tab', self::class . '::update_settings' );
	}

	/**
	 * Adds a new array element to the settings array.
	 *
	 * @param $settings_tabs array Settings tabs
	 *
	 * @return array Modified settings tabs.
	 */
	public static function add_settings_tab( array $settings_tabs ): array {
		$settings_tabs['ilogistic_settings_tab'] = __( 'iLogistic', 'woocommerce-settings-tab-ilogistic' );

		return $settings_tabs;
	}

	/**
	 * Registers what are the fields.
	 */
	public static function settings_tab() {
		woocommerce_admin_fields( self::get_settings() );
	}

	/**
	 * Registers what fields should be updating on save.
	 */
	public static function update_settings() {
		woocommerce_update_options( self::get_settings() );

		$product = get_option( 'ilogistic_cron_product_timing' ) ?? 'every_5_min';
		$order   = get_option( 'ilogistic_cron_order_timing' ) ?? 'every_5_min';

		Cron::clearCrons();
		Cron::setToOrder( $order );
		Cron::setToProduct( $product );
		Cron::createIssuaAuthTokenCron();
		Cron::createPushAllProductsCron();
	}

	/**
	 * Contains the settings for the tabs.
	 * @return array<string, array<string, string>> Returns the filtered settings
	 */
	public static function get_settings(): array {
		$order_statuses = wc_get_order_statuses();
		$settings       = array(
			'section_title'                         => array(
				'name' => 'iLogistic Token beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_token_section_title'
			),
			'ilogistic_api_key'                     => array(
				'name' => 'API kulcs',
				'type' => 'text',
				'desc' => 'A beépülő plugin használatához, szükség van egy iLogistic API kulcsra ' .
				          'melyet a következő url-el érhet el: https://ilogistic.eu/webshopapi',
				'id'   => 'ilogistic_token'
			),
			'ilogistic_shop_alias'                  => array(
				'name' => 'Webshop Alias',
				'type' => 'text',
				'desc' => 'Az ilogistic rendszerébe bekerülő rendeléseinek forrása ezzel az értékkel fog mentésre kerülni.' .
				          'Ez segítséget nyújt amennyiben egy ilogistic fiókkal több webshoppot szeretne kezelni',
				'id'   => 'ilogistic_alias'
			),
			'section_end'                           => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_token_section_end'
			),
			'status_section_title'                  => array(
				'name' => 'iLogistic státusz beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_status_section_title'
			),
			'ilogistic_status_1'                    => array(
				'name'    => 'Jóváhagyva/Jóváhagyásra vár',
				'type'    => 'multiselect',
				'options' => $order_statuses,
				'desc'    => 'A `Jóváhagyva/Jóváhagyásra vár` iLogistic státuszban lévő rendelésekkel ' .
				             'egyenértékű státusz',
				'id'      => 'ilogistic_status_jovahagyva'
			),
			'ilogistic_status_2'                    => array(
				'name'    => 'Összekészítés alatt',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Összekészítés alatt` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_osszekeszites'
			),
			'ilogistic_status_3'                    => array(
				'name'    => 'Csomagolva',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Csomagolva` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_csomagolva'
			),
			'ilogistic_status_4'                    => array(
				'name'    => 'Futárnak átadva',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Futárnak átadva` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_futarnak'
			),
			'ilogistic_status_5'                    => array(
				'name'    => 'Csomagponton várakozik',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Csomagponton várakozik` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_csomagpont'
			),
			'ilogistic_status_6'                    => array(
				'name'    => 'Szállítás közben megsérült',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Szállítás közben megsérült` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_serult'
			),
			'ilogistic_status_7'                    => array(
				'name'    => 'Kiszállítva',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Kiszállítva` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_kiszallitva'
			),
			'ilogistic_status_8'                    => array(
				'name'    => 'Visszáru',
				'type'    => 'select',
				'options' => $order_statuses,
				'desc'    => 'A `Visszáru` iLogistic státuszban lévő rendelésekkel egyenértékű státusz',
				'id'      => 'ilogistic_status_visszaru'
			),
			'status_section_end'                    => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_status_section_end'
			),
			'shipping_section_title'                => array(
				'name' => 'iLogistic szállítási mód beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_status_section_title'
			),
			'ilogistic_shipping_gls'                => array(
				'name' => 'GLS',
				'type' => 'text',
				'desc' => 'A `GLS` iLogistic szállítási módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_gls'
			),
			'ilogistic_shipping_foxpost'            => array(
				'name' => 'Foxpost',
				'type' => 'text',
				'desc' => 'A `Foxpost` iLogistic szállítási módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.' .
				          'Figyelem!: ennek megfeleltetett rendeléseknek tartalmaznia kell a ' .
				          '`foxpost_woo_parcel_apt_id` egyedi mezőt melyet a Foxpost plugin tölt ki!',
				'id'   => 'ilogistic_shipping_foxpost'
			),
			'ilogistic_shipping_szemelyes'          => array(
				'name' => 'Személyes átvétel',
				'type' => 'text',
				'desc' => 'A `Személyes átvétel` iLogistic szállítási módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_szemelyes'
			),
			'ilogistic_shipping_pactic1'            => array(
				'name' => 'Pactic Elsődleges',
				'type' => 'text',
				'desc' => 'A `Pactic Elsődleges` iLogistic szállítási módnak megfeleltetett szállítási módok `,`' .
				          ' karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_pactic1'
			),
			'ilogistic_shipping_pactic2'            => array(
				'name' => 'Pactic Másodlagos',
				'type' => 'text',
				'desc' => 'A `Pactic Másodlagos` iLogistic szállítási módnak megfeleltetett szállítási módok `,`' .
				          ' karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_pactic2'
			),
			'ilogistic_shipping_packpoint'          => array(
				'name' => 'iLogistic Csomagpont',
				'type' => 'text',
				'desc' => 'A `iLogistic csomagpont` iLogistic szállítási módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_packpoint'
			),
			'ilogistic_shipping_dhl'                => array(
				'name' => 'DHL',
				'type' => 'text',
				'desc' => 'A `DHL` iLogistic szállítási módnak megfeleltetett szállítási módok,`,` karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_dhl'
			),
			'ilogistic_shipping_MPL'                => array(
				'name' => 'MPL',
				'type' => 'text',
				'desc' => 'A `MPL` iLogistic szállítási módnak megfeleltetett szállítási módok,`,` karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_mpl'
			),
			'ilogistic_shipping_raklap'             => array(
				'name' => 'Raklapos szállítás',
				'type' => 'text',
				'desc' => 'A `Raklapos szállítás` iLogistic szállítási módnak megfeleltetett szállítási módok, `,`' .
				          ' karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_raklap'
			),
			'ilogistic_shipping_express_one'        => array(
				'name' => 'Express One',
				'type' => 'text',
				'desc' => 'Az `Express One` iLogistic szállítási módnak megfeleltetett szállítási módok, `,`' .
				          ' karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_express_one'
			),
			'ilogistic_shipping_express_one_pallet' => array(
				'name' => 'Express One Raklapos',
				'type' => 'text',
				'desc' => 'Az `Express One Raklapos` iLogistic szállítási módnak megfeleltetett szállítási módok, `,`' .
				          ' karakterrel tagolva.',
				'id'   => 'ilogistic_shipping_express_one_pallet'
			),
			'shipping_section_end'                  => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_shipping_section_end'
			),
			'payment_section_title'                 => array(
				'name' => 'iLogistic fizetési mód beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'wc_settings_tab_ilogistic_status_section_title'
			),
			'ilogistic_payment_card'                => array(
				'name' => 'Kártyás fizetés',
				'type' => 'text',
				'desc' => 'A `Kártyás fizetés` iLogistic fizetési módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_payment_card'
			),
			'ilogistic_payment_cod'                 => array(
				'name' => 'Utánvétes fizetés',
				'type' => 'text',
				'desc' => 'A `Utánvétes fizetés` iLogistic fizetési módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_payment_cod'
			),
			'ilogistic_payment_transfer'            => array(
				'name' => 'Átutalásos fizetés',
				'type' => 'text',
				'desc' => 'A `Átutalásos fizetés` iLogistic fizetési módnak megfeleltetett szállítási módok,' .
				          ' `,` karakterrel tagolva.',
				'id'   => 'ilogistic_payment_transfer'
			),
			'payment_section_end'                   => array(
				'type' => 'sectionend',
				'id'   => 'wc_settings_tab_ilogistic_payment_section_end'
			),
			'cron_section_start'                    => array(
				'name' => 'Frissítési gyakoriság beállítások',
				'type' => 'title',
				'desc' => '',
				'id'   => 'cron_section_start'
			),

			'ilogistic_cron_order_timing' => array(
				'name'    => 'Megrendelés frissítése',
				'type'    => 'select',
				'options' => Cron::getForHtmlSelect(),
				'desc'    => 'Megrendelés frissítése megadott időközönként.',
				'id'      => 'ilogistic_cron_order_timing'
			),

			'ilogistic_cron_product_timing' => array(
				'name'    => 'Termék frissítése',
				'type'    => 'select',
				'options' => Cron::getForHtmlSelect(),
				'desc'    => 'Termék frissítése megadott időközönként.',
				'id'      => 'ilogistic_cron_product_timing'
			),
			'section_cron_end'              => array(
				'type' => 'sectionend',
				'id'   => 'section_cron_end'
			),
		);

		return apply_filters( 'wc_settings_tab_ilogistic_settings', $settings );
	}

	/**
	 * Adds a new 'Beállítások' option, under the plugin on the plugins tab.
	 *
	 * @param array $actions Current links under the plugin's plugin record.
	 * @param string $plugin_file_name Plugin's name
	 *
	 * @return array Modified links of the plugin's plugin record.
	 */
	public static function add_settings_link( array $actions, string $plugin_file_name ): array {
		if (
			preg_match( '/^(ilogistic-woocommerce-plugin[a-zA-Z0-9._-]*)(\/ilogistic.php)$/', $plugin_file_name )
		) {
			array_unshift(
				$actions,
				'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=ilogistic_settings_tab' ) . '">' .
				'Beállítások</a>',
			);
		}

		return $actions;
	}


}