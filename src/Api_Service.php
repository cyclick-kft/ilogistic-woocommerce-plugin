<?php

namespace Ilogistic;

use Ilogistic\Model\Order;
use Ilogistic\Model\Product;
use WC_Logger;
use WP_Error;

class Api_Service {
	private const API_HOST_URL = 'https://api.ilogistic.eu';
	private const MULTIPLE_PRODUCTS_API_URL = self::API_HOST_URL . '/products/products';
	private const PUSH_MULTIPLE_PRODUCTS_API_URL = self::API_HOST_URL . '/products/products-multiple';
	private const MULTIPLE_ORDERS_API_URL = self::API_HOST_URL . '/orders/orders';
	private const SINGLE_ORDER_API_URL = self::API_HOST_URL . '/orders/order/';
	private const AUTHENTICATION_API_URL = self::API_HOST_URL . '/authentication/auth';
	private const TOKEN_VALIDATION_API_URL = self::API_HOST_URL . '/authentication/validate';
	private const STOCK_PRODUCTS_API_URL = self::API_HOST_URL . '/products/stock';
	private const GET_RESENT_ORDER_API_URL = self::API_HOST_URL . '/orders/resent-order';
	private static ?Api_Service $instance = null;
	private WC_Logger $logger;

	private function __construct() {
		$log_handler  = new Logfmt_Log_Handler_File();
		$this->logger = new WC_Logger( [ $log_handler ] );
	}

	public static function get_service(): Api_Service {
		if ( self::$instance === null ) {
			self::$instance = new Api_Service();
		}

		return self::$instance;
	}

	/**
	 * @param $order Order
	 * @param int $order_id
	 * @param $token string JWT token
	 * @param string|null $alias
	 *
	 * @return int|null
	 */
	public function push_order( Order $order, int $order_id, string $token, ?string $alias ): ?int {
		$body     = $order->get_as_json();
		$response = wp_remote_post(
			self::MULTIPLE_ORDERS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token, $alias ),
				'blocking'    => true,
				'body'        => $body
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				if ( str_contains( $response->get_error_message(), "cURL error 28:" ) ) {
					$this->logger->error(
						"failed to push order, got cURL error 28!",
						[ 'order_id' => $order_id ]
					);

					return null;
				}
				$this->log_error( $response->get_error_message() );
			}

			return 0;
		}

		if ( (int) $response['response']['code'] >= 500 ) {
			$this->logger->error(
				"failed to push order, got {$response['response']['code']}!",
				[ 'Error code' => $response['response']['code'], 'order_id' => $order_id ]
			);

			return null;
		}

		$unmarshalled_response = json_decode( $response['body'], true );
		$ilogistic_id          = ! is_null( $unmarshalled_response['id'] ) ? $unmarshalled_response['id'] : 0;
		if ( is_null( $unmarshalled_response ) ) {
			$this->logger->error(
				"failed to push order!",
				[ 'woo_id' => $order_id, 'error' => $response['body'] ]
			);
		} else {
			if ( isset( $unmarshalled_response['error'] ) ) {
				$this->logger->error( 'failed to push order',
					[
						'woo_id'  => $order_id,
						'ilog_id' => $ilogistic_id,
						'reason'  => $unmarshalled_response['error']
					] );
			} else {
				$this->logger->info(
					"new order pushed",
					[ 'woo_id' => $order_id, 'ilog_id' => $ilogistic_id ]
				);
			}
		}

		return $ilogistic_id;
	}

	public function get_order( string $ilog_id, string $token ): ?array {
		$response = wp_remote_get(
			self::SINGLE_ORDER_API_URL . $ilog_id,
			[
				'method'      => 'GET',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true
			]
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return null;
		} else {
			return json_decode( $response['body'], true );
		}
	}

	/**
	 * @param $product Product
	 * @param $token string JWT token
	 */
	public function push_product( Product $product, string $token ): void {
		$body     = $product->get_as_json();
		$response = wp_remote_post(
			self::MULTIPLE_PRODUCTS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true,
				'body'        => $body
			) );
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );

				return;
			}
		}
		if ( intval( $response['response']['code'] ) >= 300 ) {
			$this->logger->error( 'product push failed', [
				'sku'         => $product->itemNumber,
				'status_code' => $response['response']['code'],
				'response'    => $response['body']
			] );

			return;
		}
		$ilog_product_id = json_decode( $response["body"], true )["id"];
		$this->logger->info( "product pushed", [ 'sku' => $product->itemNumber, 'ilog_id' => $ilog_product_id ] );
	}

	/**
	 * @param $products Product[]
	 * @param $token string
	 */
	public function push_products( array $products, string $token ): void {
		$product_ids = array_reduce(
			$products,
			function ( string $accu, Product $currentProduct ) {
				return $accu . $currentProduct->itemNumber . ", ";
			},
			''
		);
		$body        = json_encode( $products, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT );
		$response    = wp_remote_post(
			self::PUSH_MULTIPLE_PRODUCTS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 20,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true,
				'body'        => $body
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return;
		}
		if ( intval( $response['response']['code'] ) >= 300 ) {
			$this->logger->error( 'products push failed', [
				'skus'        => $product_ids,
				'status_code' => $response['response']['code'],
				'response'    => $response['body']
			] );
		}
		$this->logger->info( "multiple products pushed", [ 'count' => count( $products ), 'ids' => $product_ids ] );
	}

	/**
	 * @param Order $order
	 * @param string $ilog_id
	 * @param string $token
	 */
	public function update_order( Order $order, string $ilog_id, string $token ): void {
		$response = wp_remote_post(
			self::SINGLE_ORDER_API_URL . $ilog_id,
			array(
				'method'      => 'PATCH',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true,
				'body'        => $order->get_as_json()
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );

				return;
			}
		}
		if ( intval( $response['response']['code'] ) >= 300 ) {
			$this->logger->error( 'order update failed', [
				'woo_id'      => $order->foreignId,
				'ilog_id'     => $ilog_id,
				'status_code' => $response['response']['code'],
				'response'    => $response['body']
			] );

			return;
		}
		$this->logger->info(
			"order content updated",
			[ 'woo_id' => $order->foreignId, 'ilog_id' => $ilog_id ]
		);
	}

	public function get_new_token( string $token ): string {
		$response = wp_remote_get(
			self::AUTHENTICATION_API_URL,
			array(
				'method'      => 'GET',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => [ 'Authorization' => 'Bearer ' . $token, 'Accept' => 'application/json' ],
				'blocking'    => true,
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}
			$this->logger->critical(
				'Failed to pull a new token form iLogistic, yours will expire soon. ' .
				'Please contact the developers or generate a new token manually!'
			);

			return $token;
		}
		$unmarshalled_response = json_decode( $response['body'], true );
		$this->logger->info( 'Your Authentication token renewed automatically.' );
		if ( isset( $unmarshalled_response['error'] ) ) {
			$this->logger->critical(
				'failed to pull a new token and yours will expire soon, please generate a new token manually!',
				[ 'error' => $unmarshalled_response['error'] ]
			);
		}

		return $unmarshalled_response['token'] ?? '';
	}

	public function delete_order( string $ilog_id, string $token ): void {
		$response = wp_remote_post(
			self::SINGLE_ORDER_API_URL . $ilog_id,
			array(
				'method'      => 'DELETE',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => [ 'Authorization' => 'Bearer ' . $token ],
				'blocking'    => true,
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );

				return;
			}
		}
		if ( intval( $response['response']['code'] ) >= 300 ) {
			$this->logger->error( 'order delete failed', [
				'ilog_id'     => $ilog_id,
				'status_code' => $response['response']['code'],
				'response'    => $response['body']
			] );

			return;
		}
		$this->logger->info(
			"order deleted",
			[ 'ilog_id' => $ilog_id ]
		);
	}

	public function validate_token( string $token ): bool {
		$response = wp_remote_get(
			self::TOKEN_VALIDATION_API_URL,
			array(
				'method'      => 'GET',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => [ 'Authorization' => 'Bearer ' . $token ],
				'blocking'    => true
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return false;
		}
		$message = json_decode( $response['body'], true );
		if ( isset( $message['error'] ) ) {
			$this->logger->error(
				"failed to validate token",
				[ 'error' => $message['error'] ]
			);

			return false;
		} else {
			$this->logger->info( 'API token validated' );

			return true;
		}
	}

	/**
	 * Get the current stock of the given skus.
	 *
	 * @param array $updatable_products Numeric array contains updatable products
	 * @param string $token iLogistic API token
	 *
	 * @return array<string, int> Unmarshalled API response ['sku' => stock, 'sku2' => stock ...]
	 */
	public function get_stock( array $updatable_products, string $token ): array {
		$response = wp_remote_post(
			self::STOCK_PRODUCTS_API_URL,
			array(
				'method'      => 'POST',
				'timeout'     => 30,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'body'        => json_encode( $updatable_products, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE ),
				'blocking'    => true
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return array();
		}
		$stocks = json_decode( $response['body'], true );
		if ( is_null( $stocks ) ) {
			return array();
		}

		return $stocks;
	}

	/**
	 * Emits a Woocommerce error in the log with the HTTP request failure.
	 *
	 * @param string $error_message HTTP error message
	 */
	private function log_error( string $error_message ): void {
		$this->logger->critical( "failed to process HTTP request", [ 'error' => $error_message ] );
	}

	/**
	 * Parses the headers as an associative array.
	 *
	 * @param string $token Authorization token
	 *
	 * @return string[] Array of HTTP request headers
	 */
	private function prepare_headers( string $token, ?string $sourceHeader = null ): array {
		$headers = array(
			'Content-Type'  => 'application/json',
			'Accept'        => 'application/json',
			'Authorization' => 'Bearer ' . $token
		);
		if ( empty( $sourceHeader ) === false ) {
			$headers['X-WebShop-Alias'] = $sourceHeader;
		}

		return $headers;
	}

	public function get_resent_order( int $ilogistic_order_id, string $token ): ?array {
		$response = wp_remote_get(
			self::GET_RESENT_ORDER_API_URL . "/{$ilogistic_order_id}",
			array(
				'method'      => 'GET',
				'timeout'     => 10,
				'redirection' => 5,
				'headers'     => $this->prepare_headers( $token ),
				'blocking'    => true
			)
		);
		if ( $response instanceof WP_Error ) {
			if ( $response->has_errors() ) {
				$this->log_error( $response->get_error_message() );
			}

			return null;
		}

		if ( in_array( $response['response']['code'], [ 200, 400, 404 ], true ) === false ) {
			$this->logger->error(
				"failed to get resent order",
				[ 'status_code' => $response['response']['code'] ]
			);

			return null;
		}

		$message = json_decode( $response['body'], true );

		if ( $response['response']['code'] !== 200 ) {
			$this->logger->warning(
				"can't get resent order",
				[ 'error' => $message['error'] ]
			);

			return [ 'status' => 'not found' ];
		}

		return [
			'status'                 => $message['status'],
			'new_ilogistic_order_id' => $message['newOrderId'],
		];
	}
}