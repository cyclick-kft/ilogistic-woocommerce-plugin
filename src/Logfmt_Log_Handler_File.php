<?php

namespace Ilogistic;

use WC_Log_Handler_File;

class Logfmt_Log_Handler_File extends WC_Log_Handler_File {

	/**
	 * Overrides the `WC_Log_Handler_File`'s default log formatter,
	 * to format the logs according to the logfmt 'specification'.
	 *
	 * @param int $timestamp Log timestamp.
	 * @param string $level emergency|alert|critical|error|warning|notice|info|debug.
	 * @param string $message Log message.
	 * @param array $context Additional information for log handlers.
	 *
	 * @return string Formatted log entry.
	 */
	protected static function format_entry( $timestamp, $level, $message, $context ): string {
		$final_message = [
			'level'   => strtolower( $level ),
			'ts'      => date( 'Y-m-d\TH:i:s.u', $timestamp ),
			'version' => Ilogistic_Woo_App::ILOGISTIC_PLUGIN_VERSION,
			'message' => $message,
		];
		$final_message = array_merge( $final_message, $context );

		return self::parseLogfmt( $final_message );
	}

	/**
	 * Marshall the given array to the logfmt format.
	 * @see https://www.brandur.org/logfmt
	 * @see https://pkg.go.dev/github.com/go-logfmt/logfmt
	 *
	 * @param array $logFields Potential key value pairs.
	 *
	 * @return string Marshalled logfmt string line.
	 */
	private static function parseLogfmt( array $logFields ): string {
		$parsedKeyValPairs = [];
		foreach ( $logFields as $key => $val ) {
			$normalized_key = self::normalizeLogfmtKey( $key );

			if ( is_array( $val ) || is_object( $val ) ) {
				foreach ( (array) $val as $subKey => $subValue ) {
					$normalized_subkey = self::normalizeLogfmtKey( $subKey );

					$parsedKeyValPairs[] = "$normalized_key.$normalized_subkey=" . self::normalizeLogfmtValue( $val );
				}
			} else {
				$parsedKeyValPairs[] = "$normalized_key=" . self::normalizeLogfmtValue( $val === null ? '' : $val );
			}
		}

		return join( ' ', $parsedKeyValPairs );
	}

	/**
	 * Escapes and quotes the string if it is necessary, to match to the logfmt specification.
	 *
	 * @param string $value Value string to normalize
	 *
	 * @return string
	 */
	private static function normalizeLogfmtValue( string $value ): string {
		$sanitizedValue = trim( str_replace( '"', '\"', $value ) );
		if ( strpos( $sanitizedValue, ' ' ) !== false || strpos( $sanitizedValue, '=' ) !== false ) {
			$sanitizedValue = "\"$sanitizedValue\"";
		}

		return $sanitizedValue;
	}

	/**
	 * Transform the key string to lowercase and replace spaces.
	 *
	 * @param string $key Key string to normalize
	 *
	 * @return string
	 */
	private static function normalizeLogfmtKey( string $key ): string {
		return str_replace( " ", "_", strtolower( $key ) );
	}

	/**
	 * Handle a log entry.
	 *
	 * @param int $timestamp Log timestamp.
	 * @param string $level emergency|alert|critical|error|warning|notice|info|debug.
	 * @param string $message Log message.
	 * @param array $context {
	 *      Additional information for log handlers.
	 *
	 * @type string $source Optional. Determines log file to write to. Default 'log'.
	 * @type bool $_legacy Optional. Default false. True to use outdated log format
	 *         originally used in deprecated WC_Logger::add calls.
	 * }
	 *
	 * @return bool False if value was not handled and true if value was handled.
	 */
	public function handle( $timestamp, $level, $message, $context ): bool {
		$entry = self::format_entry( $timestamp, $level, $message, $context );

		return $this->add( $entry, 'iLogistic' );
	}
}