<?php

namespace Ilogistic\Model;

use Ilogistic\Logfmt_Log_Handler_File;
use WC_Logger;
use WC_Order;

/**
 * Class Order
 * @package Cyclick\Ilogistic\Model
 */
class Order {
	/** @var null|string $foreignId WC_Order ID, marked as foreignId in the iLogistic system */
	public ?string $foreignId = null;
	public array $delivery = array();
	public array $billing = array();
	public array $payment = array();
	public array $content = array();

	private static ?WC_Logger $logger = null;

	/**
	 * Order constructor.
	 *
	 * @param $input_data WC_Order|array<string, mixed> Contains a WC_Order object or and Assoc array with the API data
	 * @param $is_woocommerce_data bool Indicates if the $inputData is from the WC or not
	 */
	private function __construct( array|WC_Order $input_data, bool $is_woocommerce_data ) {
		if ( $is_woocommerce_data ) {
			$this->create_from_woocommerce_data( $input_data );
		} else {
			$this->create_from_api_data( $input_data );
		}
	}

	/**
	 * Parses the API data to self's public fields
	 *
	 * @param $input_data WC_Order Woocommerce Order object that contains the order's data
	 */
	private function create_from_woocommerce_data( WC_Order $input_data ): void {
		if ( $input_data->meta_exists( 'foxpost_woo_parcel_apt_id' ) ) {
			$parcel_number = $input_data->get_meta( 'foxpost_woo_parcel_apt_id', true );
		} elseif ( $input_data->meta_exists( '_vp_woo_pont_point_id' ) ) {
			$parcel_number = $input_data->get_meta( '_vp_woo_pont_point_id', true );
		} else {
			$parcel_number = "";
		}

		if ( $input_data->meta_exists( '_vp_woo_pont_provider' ) ) {
			$shipping_company = $input_data->get_meta( '_vp_woo_pont_provider' );

			if ( in_array( $shipping_company, [ 'gls_shop', 'gls_locker' ], true ) ) {
				$shipping_company = 'gls';
			}

			if ( strpos( $shipping_company, 'posta' ) !== false ) {
				$shipping_company = 'mpl';
			}

			if ( in_array( $shipping_company, [ 'gls', 'foxpost', 'packeta', 'mpl' ] ) === false ) {
				self::getLogger()->warning(
					"order {$this->foreignId} has possible bad shipping method: $shipping_company, please check your settings #" . __LINE__
				);
			}
		} else {
			$shipping_company = $this->prepare_shipping_method( $input_data->get_shipping_method() );
		}

		$delivery_name = $input_data->get_shipping_first_name() . " " . $input_data->get_shipping_last_name();

		if ( $delivery_name === ' ' ) {
			$delivery_name = $input_data->get_billing_first_name() . " " . $input_data->get_billing_last_name();
		}

		$this->foreignId = (string) $input_data->get_id();
		$this->delivery  = [
			"name"        => $delivery_name,
			"email"       => $input_data->get_billing_email(),
			"phoneNumber" => $input_data->get_billing_phone(),
			"country"     => $input_data->get_shipping_country(),
			"postCode"    => $input_data->get_shipping_postcode(),
			"city"        => $input_data->get_shipping_city(),
			"address"     => $input_data->get_shipping_address_1() . " " . $input_data->get_shipping_address_2(),
			"company"     => $shipping_company,
			"aptNumber"   => $parcel_number
		];
		$this->billing   = [
			"name"        => $input_data->get_billing_first_name() . " " . $input_data->get_billing_last_name(),
			"email"       => $input_data->get_billing_email(),
			"phoneNumber" => $input_data->get_billing_phone(),
			"country"     => $input_data->get_billing_country(),
			"postCode"    => $input_data->get_billing_postcode(),
			"city"        => $input_data->get_billing_city(),
			"address"     => $input_data->get_billing_address_1() . " " . $input_data->get_billing_address_2()
		];
		$this->payment   = [
			"type" => $this->prepare_payment_method( $input_data->get_payment_method(), round( $input_data->get_total(), 2 ) ),
			"cost" => round( $input_data->get_total(), 2 )
		];

		$wc_order_items = $input_data->get_items();

		if ( empty( $wc_order_items ) ) {
			self::$logger->debug( "empty order", [ 'woo_id' => $this->foreignId ] );
		}

		$this->content = [];
		foreach ( $wc_order_items as $item ) {
			$product_data = wc_get_product( $item->get_data()['product_id'] );

			if ( empty( $item->get_data()['variation_id'] ) === false ) {
				$child_product = wc_get_product( $item->get_data()['variation_id'] );

				$this->content[ $child_product->get_sku() ] = [
					"itemNumber" => $child_product->get_sku(),
					"quantity"   => isset( $this->content[ $child_product->get_sku() ]['quantity'] ) ?
						[ $child_product->get_sku() ]['quantity'] + $item->get_quantity() :
						$item->get_quantity()
				];
			} else {
				$this->content[ $product_data->get_sku() ] = [
					"itemNumber" => $product_data->get_sku(),
					"quantity"   => isset( $this->content[ $product_data->get_sku() ]['quantity'] ) ?
						$this->content[ $product_data->get_sku() ]['quantity'] + $item->get_quantity() :
						$item->get_quantity()
				];
			}
		}
		$this->content = array_values( $this->content );
	}

	/**
	 * Parses the API data to self's public fields
	 *
	 * @param $input_data array Assoc array contains the API data
	 */
	private function create_from_api_data( array $input_data ): void {
		$this->delivery = [
			"name"        => $input_data["delivery"]["name"],
			"email"       => $input_data["delivery"]["email"],
			"phoneNumber" => $input_data["delivery"]["phoneNumber"],
			"country"     => $input_data["delivery"]["country"],
			"postCode"    => $input_data["delivery"]["postCode"],
			"city"        => $input_data["delivery"]["city"],
			"address"     => $input_data["delivery"]["address"],
			"company"     => $input_data["delivery"]["company"],
			"aptNumber"   => $input_data["delivery"]["aptNumber"]
		];
		$this->billing  = [
			"name"        => $input_data["billing"]["name"],
			"email"       => $input_data["billing"]["email"],
			"phoneNumber" => $input_data["billing"]["phoneNumber"],
			"country"     => $input_data["billing"]["country"],
			"postCode"    => $input_data["billing"]["postCode"],
			"city"        => $input_data["billing"]["city"],
			"address"     => $input_data["billing"]["address"]
		];
		$this->payment  = [
			"type" => $input_data["payment"]["type"],
			"cost" => $input_data["payment"]["cost"]
		];

		foreach ( $input_data["content"] as $item ) {
			$this->content[] = [ "itemNumber" => (string) $item["sku"], "quantity" => $item["quantity"] ];
		}
	}

	/**
	 * Creates a new Order from based on the Woocommerce data.
	 *
	 * @param $db_data WC_Order order from the WC database
	 *
	 * @return Order new Order based on the input data
	 */
	public static function create_from_woocommerce_order( WC_Order $db_data ): Order {
		return new Order( $db_data, true );
	}

	/**
	 * @param $db_data array Order that comes from the iLogistic API
	 *
	 * @return Order new Order based on the input data
	 */
	public static function create_from_api_request( array $db_data ): Order {
		return new Order( $db_data, false );
	}

	/**
	 * Returns the Order as json string, that compatible with the iLogistic API.
	 * @return string Json String
	 */
	public function get_as_json(): string {
		return json_encode( $this, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT );
	}

	/**
	 * Converts the WooCommerce shipping method to an iLogistic one, mapping by the settings fields.
	 *
	 * @param string $wc_shipping_method WooCommerce shipping method's title.
	 *
	 * @return string iLogistic compatible shipping method or the original title for backwards compatibility.
	 */
	private function prepare_shipping_method( string $wc_shipping_method ): string {
		$gls_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_gls' ) ) );

		$foxpost_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_foxpost' ) ) );

		$personal_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_szemelyes' ) ) );

		$pactic1_shippings = array_map(
			fn( $method ) => trim( $method ),
			explode( ',', get_option( 'ilogistic_shipping_pactic1' ) )
		);

		$pactic2_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_pactic2' ) ) );

		$packpoint_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_packpoint' ) ) );

		$dhl_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_dhl' ) ) );

		$mpl_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_mpl' ) ) );

		$raklap_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_raklap' ) ) );

		$express_one_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_express_one' ) ) );

		$express_one_pallet_shippings = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_shipping_express_one_pallet' ) ) );

		if ( $wc_shipping_method === '' ) {
			return 'Nem szükséges';
		}

		if ( in_array( $wc_shipping_method, $gls_shippings, true ) ) {
			return 'GLS';
		}

		if ( in_array( $wc_shipping_method, $foxpost_shippings, true ) ) {
			return 'Foxpost';
		}

		if ( in_array( $wc_shipping_method, $personal_shippings, true ) ) {
			return 'Személyes átvétel';
		}

		if ( in_array( $wc_shipping_method, $pactic1_shippings, true ) ) {
			return 'Pactic Elsődleges';
		}

		if ( in_array( $wc_shipping_method, $pactic2_shippings, true ) ) {
			return 'Pactic Másodlagos';
		}

		if ( in_array( $wc_shipping_method, $packpoint_shippings, true ) ) {
			return 'iLogistic csomagpont';
		}

		if ( in_array( $wc_shipping_method, $dhl_shippings, true ) ) {
			return 'DHL';
		}

		if ( in_array( $wc_shipping_method, $mpl_shippings, true ) ) {
			return 'MPL';
		}

		if ( in_array( $wc_shipping_method, $raklap_shippings, true ) ) {
			return 'Raklapos szállítás';
		}

		if ( in_array( $wc_shipping_method, $express_one_shippings, true ) ) {
			return 'Express One';
		}

		if ( in_array( $wc_shipping_method, $express_one_pallet_shippings, true ) ) {
			return 'Express One Raklapos';
		}

		self::getLogger()->warning(
			"order {$this->foreignId} has possible bad shipping method: $wc_shipping_method, please check your settings #" . __LINE__
		);

		return $wc_shipping_method;
	}

	/**
	 * Converts the WooCommerce payment method to an iLogistic one, mapping by the settings fields.
	 *
	 * @param string $wc_payment_gateway WooCommerce's payment gateway.
	 *
	 * @return string iLogistic compatible payment method or the original title for backwards compatibility.
	 */
	private function prepare_payment_method( string $wc_payment_gateway, float $price ): string {
		$card_payment = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_payment_card' ) ) );

		$cod = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_payment_cod' ) ) );

		$bank_transfer = array_map( function ( $method ) {
			return trim( $method );
		}, explode( ',', get_option( 'ilogistic_payment_transfer' ) ) );

		$payment_gateways = WC()->payment_gateways()->payment_gateways();

		if ( isset( $payment_gateways[ $wc_payment_gateway ] ) ) {
			$payment_method = $payment_gateways[ $wc_payment_gateway ]->title;

			if ( in_array( $payment_method, $card_payment, true ) ) {
				return 'Bankkártyás fizetés';
			}
			if ( in_array( $payment_method, $cod, true ) ) {
				return 'Utánvétes fizetés';
			}
			if ( in_array( $payment_method, $bank_transfer, true ) ) {
				return 'Átutalásos fizetés';
			}
		}

		if ( $price <= 0 ) {
			return 'Utánvétes fizetés';
		}

		self::getLogger()->warning(
			"order {$this->foreignId} has possible bad payment method, please check your settings."
		);

		return $wc_payment_gateway;
	}

	/**
	 * Creates a new WC_Logger or returns and existing one.
	 * @return WC_Logger
	 */
	private static function getLogger(): WC_Logger {
		if ( is_null( self::$logger ) ) {
			$log_handler  = new Logfmt_Log_Handler_File();
			self::$logger = new WC_Logger( [ $log_handler ] );
		}

		return self::$logger;
	}

}