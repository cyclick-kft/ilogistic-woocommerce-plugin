<?php

namespace Ilogistic\Model;

use WC_Product;

class Product {
	public string $itemNumber;
	public array $barCode;
	public string $name;
	public string $type;
	public string $category;
	public int $weight;
	public int $width;
	public int $height;
	public int $depth;
	public int $fragile;
	public int $price;
	public int $tax;
	public int $critical;
	public string $status;
	public bool $hasSerial;
	public bool $hasExpirationDate;

	/**
	 * @param $wc_product WC_Product Product from the WooCommerce database.
	 */
	private function __construct( WC_Product $wc_product ) {
		$categoryList         = wc_get_product_category_list( $wc_product->get_id() );
		$categoryFirstElement = explode( '>', $categoryList )[1] ?? '';

		$this->itemNumber        = $wc_product->get_sku();
		$this->barCode           = [];
		$this->name              = $wc_product->get_name();
		$this->type              = rtrim( $categoryFirstElement, '</a' );
		$this->category          = $wc_product->is_virtual() ? 'virtual' : 'termek';
		$this->status            = "active";
		$this->weight            = intval( $wc_product->get_weight() );
		$this->width             = intval( $wc_product->get_width() );
		$this->height            = intval( $wc_product->get_height() );
		$this->depth             = intval( $wc_product->get_length() );
		$this->fragile           = 1;
		$this->price             = intval( $wc_product->get_price() );
		$this->tax               = 27;
		$this->critical          = 0;
		$this->hasSerial         = false;
		$this->hasExpirationDate = false;
	}

	/**
	 * Creates a new Product from based on the Woocommerce data.
	 *
	 * @param $wc_product WC_Product product from the WC database
	 *
	 * @return Product new Product based on the input data
	 */
	public static function create_from_woocommerce_product( WC_Product $wc_product ): Product {
		return new Product( $wc_product );
	}

	/**
	 * Returns the Product as json string, that compatible with the iLogistic API.
	 * @return string Json String
	 */
	public function get_as_json(): string {
		return json_encode( $this, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT );
	}

}